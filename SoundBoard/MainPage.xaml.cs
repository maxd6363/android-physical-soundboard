﻿using Android.Widget;
using Plugin.Maui.Audio;
using Application = Android.App.Application;

namespace SoundBoard;

public partial class MainPage : ContentPage
{

    private Dictionary<string, string> Sounds { get; }

    public MainPage()
    {
        InitializeComponent();
        Sounds = new Dictionary<string, string>
        {
            { "A", "accel.mp3" },
            { "Z", "catch.mp3" },
            { "E", "catch2.mp3" },
            { "R", "chime.mp3" },
            { "T", "coolvibration.mp3" },
            { "Y", "dejavu.mp3" },
            { "U", "engine1.mp3" },
            { "I", "startup.mp3" },
            { "O", "tut.mp3" }
        };

        Loaded += MainPage_Loaded;

    }

    private void MainPage_Loaded(object sender, EventArgs e)
    {
        InputText.Focus();
    }

    private void InputView_OnTextChanged(object sender, TextChangedEventArgs e)
    {
        var value = $"{e.NewTextValue.LastOrDefault()}";
        var found = Sounds.TryGetValue(value.ToUpper(), out var sound);

        sound = found ? sound : "No sound !";

        InputLabel.Text = $"{value} -> {sound} !";
        if (found)
            PlaySound(sound);
    }

    private void PlaySound(string sound)
    {
        try
        {
            var player = AudioManager.Current.CreatePlayer(FileSystem.OpenAppPackageFileAsync(sound).Result);
            player.Play();
        }
        catch (Exception)
        {
            // ignored
        }
    }
}

